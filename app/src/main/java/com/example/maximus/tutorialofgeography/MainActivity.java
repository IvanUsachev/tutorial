package com.example.maximus.tutorialofgeography;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    Button button;
    EditText editText;
    TextView textView, timer, capital, number, result;
    Subscription s;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        capital = (TextView) findViewById(R.id.capital_name);
        button = (Button) findViewById(R.id.button);
        editText = (EditText) findViewById(R.id.editText);
        textView = (TextView) findViewById(R.id.textView);
        timer = (TextView) findViewById(R.id.textView2);
        number = (TextView) findViewById(R.id.number);
        result = (TextView) findViewById(R.id.final_result);


        final Random random = new Random();

        int firstRandomNumber = random.nextInt(50);
        int secondRandomNumber = random.nextInt(50);
        int thirdRandomNumber = random.nextInt(50);
        int fourthRandomNumber = random.nextInt(50);

        Retrofit.getCountries(new Callback<List<Country>>() {
            @Override
            public void success(List<Country> countries, Response response) {


                final String capitalCountry = editText.getText().toString();
                number.setText("1");
                capital.setText(countries.get(firstRandomNumber).name);
//                editText.setText(countries.get(firstRandomNumber).capital.toString());
                final String myCapital = countries.get(firstRandomNumber).capital.toString();


                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (myCapital.equalsIgnoreCase(editText.getText().toString())) {
                            result.setText("your answer is correct");
                            result.setTextColor(Color.BLUE);

                            FileOutputStream fos = null;
                            try {
                                fos = openFileOutput("file_name", Context.MODE_PRIVATE);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                            try {
                                fos.write(editText.getText().toString().getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                        } else {
                            result.setText("your answer is wrong");
                            result.setTextColor(Color.RED);
                        }

                        number.setText("2");

                        s = rx.Observable.interval(1, TimeUnit.SECONDS)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe((Long aLong) ->
                                {
                                    Log.d("Timer", "tick");
                                    timer.setText(aLong.toString());
                                });


                        capital.setText(countries.get(secondRandomNumber).name);
//                        editText.setText(countries.get(secondRandomNumber).capital.toString());
                        final String myCapital = countries.get(secondRandomNumber).capital.toString();


                        button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (myCapital.equalsIgnoreCase(editText.getText().toString())) {
                                    result.setText("your answer is correct");
                                    result.setTextColor(Color.BLUE);

                                    FileOutputStream fos = null;
                                    try {
                                        fos = openFileOutput("file_name", Context.MODE_PRIVATE);
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        fos.write(editText.getText().toString().getBytes());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        fos.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }


                                } else {
                                    result.setText("your answer is wrong");
                                    result.setTextColor(Color.RED);
                                }


                                number.setText("3");

                                s.unsubscribe();
                                s = null;
                                s = rx.Observable.interval(1, TimeUnit.SECONDS)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe((Long aLong) ->
                                        {
                                            Log.d("Timer", "tick");
                                            timer.setText(aLong.toString());
                                        });


                                capital.setText(countries.get(thirdRandomNumber).name);
                                editText.setText(countries.get(thirdRandomNumber).capital.toString());
                                final String myCapital = countries.get(thirdRandomNumber).capital.toString();



                                button.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        if (myCapital.equalsIgnoreCase(editText.getText().toString())) {
                                            result.setText("your answer is correct");
                                            result.setTextColor(Color.BLUE);

                                            FileOutputStream fos = null;
                                            try {
                                                fos = openFileOutput("file_name", Context.MODE_PRIVATE);
                                            } catch (FileNotFoundException e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                fos.write(editText.getText().toString().getBytes());
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                fos.close();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }


                                        } else {
                                            result.setText("your answer is wrong");
                                            result.setTextColor(Color.RED);
                                        }


                                        number.setText("4");

                                        s.unsubscribe();
                                        s = null;

                                        s = rx.Observable.interval(1, TimeUnit.SECONDS)
                                                .subscribeOn(Schedulers.newThread())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe((Long aLong) ->
                                                {
                                                    Log.d("Timer", "tick");
                                                    timer.setText(aLong.toString());
                                                });


                                        capital.setText(countries.get(fourthRandomNumber).name);
                                        editText.setText(countries.get(fourthRandomNumber).capital.toString());
                                        final String myCapital = countries.get(fourthRandomNumber).capital.toString();

                                        final String capitalCountry = editText.getText().toString();
                                        if (capitalCountry.equalsIgnoreCase(myCapital)) {
                                            result.setText("your answer is correct");
                                        } else {
                                            result.setText("your answer is wrong");
                                        }


                                        FileInputStream fis = null;
                                        try {
                                            fis = openFileInput("file_name");
                                            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
                                            StringBuilder text = new StringBuilder();
                                            String line;
                                            while ((line = reader.readLine()) != null) {
                                                text.append(line);
                                            }
//                                            test.setText(text);
                                        } catch (FileNotFoundException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            fis.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }


                                    }
                                });


                            }
                        });
                    }
                });


            }


            @Override
            public void failure(RetrofitError error) {
//
            }
        });

    }
}