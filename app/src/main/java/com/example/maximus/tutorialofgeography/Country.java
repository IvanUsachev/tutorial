package com.example.maximus.tutorialofgeography;

import com.google.gson.annotations.SerializedName;

public class Country {


    //        @SerializedName("name")
    String name;
    //    @SerializedName("capital")
    String capital;
    //    @SerializedName("name")


    public Country(String name, String capital) {
        this.name = name;
        this.capital = capital;
    }
}
